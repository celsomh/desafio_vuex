import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    //Variable "globales".
    compras: 0,
    poblacion: 0,
    area: 0,
    paises: [],
    likes: 0,
    likeList: [],
    paisesAgregados: []
  },
  getters: {
    /*Este Getter calcula una nuevo dato a partir
    de las variables establecidas.*/
    promedio(state) {
      if (state.compras === 0) {
        return 0
      }
      return parseInt(state.poblacion / state.compras)
    },
    ultimosPaisesAgregados(state) {
      if (state.paisesAgregados.length <= 5) {
        return state.paisesAgregados
      } else {
        let inicio = state.paisesAgregados.length - 5
        let fin = state.paisesAgregados.length
        return state.paisesAgregados.slice(inicio, fin)
      }
    }
  },
  mutations: {
    nuevaCompra(state, precio) {
      state.compras = state.compras + 1
      state.poblacion = state.poblacion + precio
    },
    agregarArea(state, area) {
      state.area = state.area + area
    },
    getPaises(state, paises) {
      state.paises = paises
    },
    setLikes(state, index) {
      let isLike=!state.likeList[index]
      console.log(isLike)
      state.likeList[index]= isLike
      if (isLike)
        state.likes++
      else
        state.likes--
    
    },
    agregarPais(state, index) {
      state.paisesAgregados.push(state.paises[index])
      console.log(state.paisesAgregados);
    },
    limpiarDatos(state) {
      state.compras = 0
      state.poblacion = 0
      state.area = 0
      state.likes = 0
      state.paisesAgregados = []

    }
  },

  actions: {
    /*Se crea un action con el objetivo de obtener los países(ConsumirlaAPI),
    mediante un GET*/
    //Metodo Asincronico.
    async getAllCountries(context) {
      return await axios.get('https://restcountries.eu/rest/v2/regionalbloc/usan')
        .then(response => {
          let paises = response.data
          context.commit('getPaises', paises)
        })
    }
  }
})
