import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'
import Paises from '../views/Vuex'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/paises',
    name: 'Paises',
    component: Paises
  }
]

const router = new VueRouter({
  routes
})

export default router
